﻿using UnityEngine;
using System.Collections;

public class FP_Shooting : MonoBehaviour {

    public GameObject bullet_prefab;
    private float bulletImpulse = 100.0f;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

	    if (Input.GetButtonDown("Fire1"))
	    {
	        Camera c = Camera.main;
	        GameObject bullet = (GameObject)Instantiate(bullet_prefab, c.transform.position + c.transform.forward, c.transform.rotation);
	        bullet.GetComponent<Rigidbody>().AddForce(c.transform.forward * bulletImpulse, ForceMode.Impulse);
	    }
	}
}
