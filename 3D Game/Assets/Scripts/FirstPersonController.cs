﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class FirstPersonController : MonoBehaviour {

    //public float movementSpeed = 10.0f;
    //public float jumpSpeed = 10.0f;
    public float mouseSensitivity = 1.5f;

    private float verticalRotation = 0;
    public float pitchRange = 60.0f;

    private float verticalVelocity = 0;

    private CharacterController characterController;
    // Use this for initialization
    void Start() {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update() {
        //rotation
        transform.Rotate(0, Input.GetAxis("Mouse X") * mouseSensitivity, 0);
        verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        verticalRotation = Mathf.Clamp(verticalRotation, -pitchRange, pitchRange);
        Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);

        //movement
        float forwardSpeed = Input.GetAxis("Vertical");
        float strafeSpeed = Input.GetAxis("Horizontal");

        float jumpSpeed = GetComponentInParent<PlayerStats>().JumpSpeed;
        verticalVelocity += Physics.gravity.y * Time.deltaTime;
        if (Input.GetButtonDown("Jump") && characterController.isGrounded) {
            verticalVelocity = jumpSpeed;
        }

        float movementSpeed = GetComponentInParent<PlayerStats>().MovementSpeed;
        Vector3 speed = new Vector3(strafeSpeed * movementSpeed, verticalVelocity, forwardSpeed * movementSpeed);

        speed = transform.rotation * speed;

        characterController.Move(speed * Time.deltaTime);
    }
}
