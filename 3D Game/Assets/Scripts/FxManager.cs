﻿using UnityEngine;
using System.Collections;

public class FxManager : MonoBehaviour {

    public GameObject sniperBulletFX;

    [RPC]
    void SniperBulletFX(Vector3 startPosition, Vector3 endPosition) {

        startPosition.y = startPosition.y - 0.5f;

        GameObject sniperFx = (GameObject)Instantiate(sniperBulletFX, startPosition, Quaternion.LookRotation(Camera.main.transform.forward));
        LineRenderer lineFx = sniperFx.transform.Find("LineFX").GetComponent<LineRenderer>();
        lineFx.SetPosition(0, startPosition);
        lineFx.SetPosition(1, endPosition);
    }
}
