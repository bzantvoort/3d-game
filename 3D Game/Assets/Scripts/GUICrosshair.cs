﻿using UnityEngine;
using System.Collections;

public class GUICrosshair : MonoBehaviour {

    public Texture2D crosshairImage;
    public Rect position = new Rect(0, 0, 0, 0);
    public static bool OriginalOn = true;

    // Use this for initialization
    void Start() {
    }

    void Update() {

    }

    void OnGUI() {
        float xMin = (Screen.width / 2) - (crosshairImage.width / 2);
        float yMin = (Screen.height / 2) - (crosshairImage.height / 2);
        GUI.DrawTexture(new Rect(xMin, yMin, crosshairImage.width, crosshairImage.height), crosshairImage);
    }
}
