﻿using System;
using UnityEngine;
using System.Collections;
using JetBrains.Annotations;

public class Health : MonoBehaviour {

    public float hitPoints;
    private float currentHitPoints;
    public float respawnTime = 0f;

    private NetworkManager networkManager;

    void Start() {
        //hitPoints = GetComponentInParent<PlayerStats>().Health;

        currentHitPoints = hitPoints;

        networkManager = FindObjectOfType<NetworkManager>();
    }

    [RPC]
    public void ReceiveDamage(float damage) {
        currentHitPoints -= damage;
        Debug.Log(String.Format("{0}: {1} Health", this.name, currentHitPoints));

        if (currentHitPoints <= 0) {
            Die();
        }
    }

    //void OnGUI() {
    //    if (GetComponent<PhotonView>().isMine && gameObject.tag == "Player") {
    //        if (GUI.Button(new Rect(Screen.width - 50, 0, 50, 20), "Kill")) {
    //            Die();
    //        }
    //    }
    //}

    void Die() {
        if (GetComponent<PhotonView>().instantiationId == 0) {
            Destroy(gameObject);
        }
        else {
            if (GetComponent<PhotonView>().isMine) {
                if (gameObject.tag == "Player") {
                    networkManager.respawnTimer = respawnTime;
                    networkManager.StandByCamera.SetActive(true);
                }

                PhotonNetwork.Destroy(gameObject);
            }
        }
    }
}
