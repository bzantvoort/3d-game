﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GUITexture))]
public class HoveringTexture : MonoBehaviour {

    public Transform target;
    public Vector3 offset = Vector3.up;
    public bool clampToScreen = false;
    public float clampBorderSize = 0.05f;
    private Transform thisTransform;
    private Transform camtTransform;

    // Use this for initialization
    void Start() {
        thisTransform = transform;
        camtTransform = Camera.main.transform;
    }

    // Update is called once per frame
    void Update() {
        if (clampToScreen) {
            var relativePosition = camtTransform.InverseTransformPoint(target.position);
            relativePosition.z = Mathf.Max(relativePosition.z, 1.0f);
            thisTransform.position =
                Camera.main.WorldToViewportPoint(camtTransform.TransformPoint(relativePosition + offset));
            thisTransform.position =
                new Vector3(Mathf.Clamp(thisTransform.position.x, clampBorderSize, 1.0f - clampBorderSize),
                    Mathf.Clamp(thisTransform.position.y, clampBorderSize, 1.0f - clampBorderSize),
                    thisTransform.position.z);
        }
        else {
            thisTransform.position = Camera.main.WorldToViewportPoint(target
                .position + offset);
        }
    }
}
