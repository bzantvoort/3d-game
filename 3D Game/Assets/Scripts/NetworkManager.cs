﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class NetworkManager : MonoBehaviour {
    public string networkVersion = "0.01";
    public GameObject StandByCamera;
    public float respawnTimer = 0;
    private SpawnSpot[] spawns;

    public bool offlineMode = false;

    private bool connecting = false;

    private List<string> chatMessages;
    public int maxChatMessaages = 5;

    private PhotonView photonView;

    // Use this for initialization
    void Start() {
        photonView = GetComponent<PhotonView>();
        if (photonView == null) {
            Debug.LogError("PhotonView is missing!!!!!!!?!?!?!");
        }
        spawns = FindObjectsOfType<SpawnSpot>();
        PhotonNetwork.player.name = PlayerPrefs.GetString("Username", "Unnamed Player");
        chatMessages = new List<string>();
    }

    void OnDestroy() {
        PlayerPrefs.SetString("Username", PhotonNetwork.player.name);
    }

    public void AddChatMessage(string message) {

        photonView.RPC("AddChatMessage_RPC", PhotonTargets.AllBuffered, message);
    }

    [RPC]
    void AddChatMessage_RPC(string message) {
        while (chatMessages.Count >= maxChatMessaages) {
            chatMessages.RemoveAt(0);
        }
        chatMessages.Add(message);
    }

    void Connect() {
        PhotonNetwork.ConnectUsingSettings(networkVersion);
    }

    void OnGUI() {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());

        if (!PhotonNetwork.connected && !connecting) {
            GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Username: ");
            PhotonNetwork.player.name = GUILayout.TextField(PhotonNetwork.player.name);

            GUILayout.EndHorizontal();

            if (GUILayout.Button("Single Player")) {
                connecting = true;
                PhotonNetwork.offlineMode = true;
                OnJoinedLobby();
            }

            if (GUILayout.Button("Multiplayer")) {
                connecting = true;
                Connect();
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        if (PhotonNetwork.connected && !connecting) {
            GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();

            foreach (string message in chatMessages) {
                GUILayout.Label(message);
            }

            GUILayout.EndVertical();
            GUILayout.EndArea();
        }
    }

    void OnJoinedLobby() {
        PhotonNetwork.JoinRandomRoom();
    }

    void OnPhotonRandomJoinFailed() {
        PhotonNetwork.CreateRoom(null);
    }

    void OnJoinedRoom() {
        Debug.Log("Joined Room");

        connecting = false;
        SpawnMyPlayer();
    }


    void SpawnMyPlayer() {
        AddChatMessage(String.Format("{0} joined the game!", PhotonNetwork.player.name));

        SpawnSpot mySpawn = spawns[Random.Range(0, spawns.Length)];

        GameObject myPlayer = PhotonNetwork.Instantiate("PlayerController", mySpawn.transform.position, mySpawn.transform.rotation, 0);
        StandByCamera.SetActive(false);

        myPlayer.GetComponent<PlayerMovement>().enabled = true;
        myPlayer.GetComponent<PerformAttack>().enabled = true;
        myPlayer.transform.FindChild("Main Camera").gameObject.SetActive(true);
    }

    void Update() {
        if (respawnTimer > 0) {
            respawnTimer -= Time.deltaTime;

            if (respawnTimer <= 0) {
                SpawnMyPlayer();
            }
        }

    }
}
