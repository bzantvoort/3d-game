﻿using UnityEngine;

public class PerformAttack : MonoBehaviour {

    public float range = 100.0f;
    public float fireRate = 0.5f;
    private float cooldown = 0;
    public float damage = 20.0f;

    private FxManager fxManager;
    public GameObject debrisPrefab;

    // Use this for initialization
    void Start() {
        fxManager = GameObject.FindObjectOfType<FxManager>();

        if (fxManager == null) {
            Debug.LogError("Couldn't find FXManager");
        }
    }

    // Update is called once per frame
    void Update() {
        cooldown -= Time.deltaTime;

        if (Input.GetButton("Fire1") && cooldown <= 0) {
            Fire();
        }
    }

    void Fire() {
        if (cooldown > 0) {
            return;
        }

        Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        Transform hitTransform;
        Vector3 hitPoint = Vector3.zero;
        hitTransform = findClosestHitObject(ray, out hitPoint);

        if (hitTransform != null) {
            Health health = hitTransform.transform.GetComponent<Health>();

            while (health == null && hitTransform.parent) {
                hitTransform = hitTransform.parent;
                health = hitTransform.transform.GetComponent<Health>();
            }

            if (health != null) {
                health.GetComponent<PhotonView>().RPC("ReceiveDamage", PhotonTargets.AllBuffered, damage);
                //Same as -> health.ReceiveDamage(damage);
                //but over network
            }

            if (fxManager != null) {
                fxManager.GetComponent<PhotonView>()
                    .RPC("SniperBulletFX", PhotonTargets.All, Camera.main.transform.position, hitPoint);
            }
        }
        else {
            if (fxManager != null) {
                hitPoint = Camera.main.transform.position + (Camera.main.transform.forward * range);
                fxManager.GetComponent<PhotonView>().RPC("SniperBulletFX", PhotonTargets.All, Camera.main.transform.position, hitPoint);
            }
        }
        cooldown = fireRate;
    }

    Transform findClosestHitObject(Ray ray, out Vector3 hitPoint) {
        RaycastHit[] hits = Physics.RaycastAll(ray, range);

        Transform closestHit = null;
        float distance = 0;
        hitPoint = Vector3.zero;

        foreach (RaycastHit hit in hits) {
            if (hit.transform != this.transform && (closestHit == null || hit.distance < distance)) {
                closestHit = hit.transform;
                distance = hit.distance;
                hitPoint = hit.point;
            }
        }

        return closestHit;
    }
}
