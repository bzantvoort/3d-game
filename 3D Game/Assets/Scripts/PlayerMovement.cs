﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour {

    public float speed = 10.0f;
    public float jumpSpeed = 1.0f;
    private Vector3 direction = Vector3.zero;
    private float verticalVelocity = 0;

    private CharacterController cc;
    private Animator anim;

    public float mouseSensitivity = 1.5f;
    private float verticalRotation = 0;
    public float pitchRange = 60.0f;



    // Use this for initialization
    void Start() {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        cc = GetComponent<CharacterController>();

        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update() {

        //WASD Foward/Back/Left/Right
        direction = transform.rotation * new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        if (direction.magnitude > 1f) {
            direction = direction.normalized;
        }
        anim.SetFloat("Speed", direction.magnitude);

        // Handle jumping

        if (cc.isGrounded && Input.GetButton("Jump")) {
            verticalVelocity = jumpSpeed;
        }
    }

    void FixedUpdate() {
        //rotation
        transform.Rotate(0, Input.GetAxis("Mouse X") * mouseSensitivity, 0);
        verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        verticalRotation = Mathf.Clamp(verticalRotation, -pitchRange, pitchRange);
        Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);

        //movement
        Vector3 distance = direction * speed * Time.deltaTime;

        if (cc.isGrounded && verticalVelocity < 0) {

            anim.SetBool("Jumping", false);

            verticalVelocity = Physics.gravity.y * Time.deltaTime;
        }
        else {

            if (Mathf.Abs(verticalVelocity) > jumpSpeed * 0.75f) {
                anim.SetBool("Jumping", true);
            }

            verticalVelocity += Physics.gravity.y * Time.deltaTime;
        }

        distance.y = verticalVelocity * Time.deltaTime;
        cc.Move(distance);
    }
}
